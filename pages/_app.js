import Layout from '../comps/Layout'
import '../styles/globals.css'
import {Provider } from "next-auth/client";
import {useState, useEffect} from 'react'

function MyApp({ Component, pageProps }) {
  const [cart, setCart] = useState([]);

  useEffect(function()
  {
    setCart(JSON.parse(localStorage.getItem("cart")));
  }, []);

  return (
    <Provider session = {pageProps.session}>
    <Layout cart={cart} setCart={setCart}>
      <Component cart={cart} setCart={setCart} {...pageProps} />
    </Layout>
    </Provider>
  )
}

export default MyApp;