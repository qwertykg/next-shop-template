import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export const getServerSideProps = async (context) => {

  function getCookie(name) {
    const value = `; ${context.req.headers.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
  }

  var items = [];
  var cart = JSON.parse(getCookie("cart"));
  for (var itemID of cart)
  {
    var itemProperties = {};

    if (items[itemID])
    {
      itemProperties = items[itemID];
      itemProperties.itemQuantity++;
    }
    else
    {
      const result = await fetch(`https://p2m4f2stt1.execute-api.af-south-1.amazonaws.com/getDatabaseEntry?id=${itemID}`);
      var item = await result.json();
      
      itemProperties = {
        itemID: item.ID,
        itemName: item.name,
        itemPrice: item.price,
        itemQuantity: 1
      };
    }

     items[itemID] = itemProperties;
  }
  var totalPrice = 0;

  var array = [];
  var uniqueCart = [...new Set(cart)];

  for (var itemID of uniqueCart)
  {
    var item = items[itemID];
    totalPrice += item.itemPrice * item.itemQuantity;
    array.push(item);
  }

  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'ZAR'
  });
  
  totalPrice = formatter.format(totalPrice);

  return {props: { cart: array , totalPrice: totalPrice, fullCartLength: cart.length}}
}

export default function Home({cart, totalPrice, fullCartLength}) {
  return (
    <>
      <Head>
        <title>Online Shop | Cart</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />

        <script src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
      </Head>
      <div>
      <div className="col-md-4 order-md-2 mb-4 margin-auto">
          <h4 className="d-flex justify-content-between align-items-center mb-3">
            <span className="text-muted">Your cart</span>
            <span className="badge badge-secondary badge-pill">{fullCartLength}</span>
          </h4>
        <ul className="list-group mb-3">
        {
          cart.map(item => {
            return (
              <li key={item.itemID} className="list-group-item d-flex justify-content-between lh-condensed">
              <div>
              <h6 className="my-0">{item.itemName}</h6>
              <small>Quantity: {item.itemQuantity}</small>
            </div>
            <span className="text-muted">R{item.itemPrice}</span>
          </li>
            );
          })
        }
        <li className="list-group-item d-flex justify-content-between">
          <span>Total (ZAR)</span>
          <strong>{totalPrice}</strong>
        </li>
      </ul>

      <button className="btn btn-primary btn-lg btn-block button-clear transform" onClick={
        function()
        {
          localStorage.clear(); 
          cookieStore.set("cart", JSON.stringify([]))
          window.location.reload();
        }}>Clear cart</button>

    </div>

    <div className="col-md-8 order-md-1 margin-auto">
    <h4 className="mb-3">Shipping address</h4>

    <form className="needs-validation" action="https://www.payfast.co.za/eng/process" name="form_f83c62a147b4f1b4a55dfa7f553f88f2" method="post" >
      <div className="row">
        <div className="col-md-6 mb-3">
          <label htmlFor="firstName">First name</label>
          <input type="text" className="form-control" id="firstName" placeholder="" required />
          <div className="invalid-feedback">
            Valid first name is required.
          </div>
        </div>
        <div className="col-md-6 mb-3">
          <label htmlFor="lastName">Last name</label>
          <input type="text" className="form-control" id="lastName" placeholder="" required />
          <div className="invalid-feedback">
            Valid last name is required.
          </div>
        </div>
      </div>

      <div className="mb-3">
        <label htmlFor="address">Address</label>
        <input type="text" className="form-control" id="address" required />
        <div className="invalid-feedback">
          Please enter your shipping address.
        </div>
      </div>

      <div className="mb-3">
        <label htmlFor="suburb">Suburb</label>
        <input type="text" className="form-control" id="suburb" required />
        <div className="invalid-feedback">
          Please enter your suburb.
        </div>
      </div>

      <div className="mb-3">
        <label htmlFor="city">City</label>
        <input type="text" className="form-control" id="city" required />
        <div className="invalid-feedback">
          Please enter your city.
        </div>
      </div>

      <div className="mb-3">
        <label htmlFor="postalCode">Postal Code</label>
        <input type="text" className="form-control" id="postalCode" required />
        <div className="invalid-feedback">
          Please enter your postal code.
        </div>
      </div>

      <div className="row">
        <div className="col-md-4 mb-3">
          <label htmlFor="state">Province</label>
          <select className="custom-select d-block w-100" id="province" required>
            <option value="">Choose...</option>
            <option>Eastern Cape</option>
            <option>Free State</option>
            <option>Gauteng</option>
            <option>KwaZulu-Natal</option>
            <option>Limpopo</option>
            <option>Mpumalanga</option>
            <option>Northern Cape</option>
            <option>North West</option>
            <option>Western Cape</option>
          </select>
          <div className="invalid-feedback">
            Please provide a valid province.
          </div>
        </div>
      </div>
      <input type="hidden" name="cmd" value="_paynow" />
      <input type="hidden" name="receiver" value="15212713" />
      <input type="hidden" name="item_name" value="My online store" />
      <input type="hidden" name="amount" value={totalPrice.split("ZAR ")[1]} />
      <input type="hidden" name="item_description" value="" />
      <input type="hidden" name="return_url" value="http://localhost:8080/pages/paymentSucessful.html" />
      <input type="hidden" name="cancel_url" value="http://localhost:8080/pages/paymentFailed.html" />
     
      <input type="submit" src="https://www.payfast.co.za/images/buttons/light-small-paynow.png" width="165" height="36" title="Pay Now with PayFast" />
    </form>
  </div>
</div>

    </>
  )
}