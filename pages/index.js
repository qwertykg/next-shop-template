import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { useEffect } from 'react';
export const getServerSideProps = async (context) => {
  const res = await fetch('https://e964mn6gw0.execute-api.af-south-1.amazonaws.com/get-all-images');
  const data = await res.json();
  var response = data.body;
  
  var items = [];
  for (var image of response)
  {
    var item = {};

    var name = image.split("/")[image.split("/").length - 1];
    const result = await fetch(`https://p2m4f2stt1.execute-api.af-south-1.amazonaws.com/getDatabaseEntry?image=${name}`);
    item = await result.json();
    item.image = image;

    items.push(item);
  }

  return {
    props: { items: items }
  }
}

export default function Home(props) {
  useEffect(function()
  {
    localStorage.setItem("cart", JSON.stringify(props.cart));
    cookieStore.set("cart", JSON.stringify(props.cart))
  });

  return (
    <>
      <Head>
        <title>Online Shop | Home</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
      </Head>
      <div>
        <h1 className={styles.title}>Online Shop Template</h1>
        <section>
        <div className="container px-4 px-lg-5 mt-5">
        <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        {
          props.items.map(item => {
            return (
              <div className="col mb-5 maxWidth" key={item.image}>
              <div className="card h-100">
                  <img className="card-img-top my-image" src={item.image}/>
                  <div className="card-body p-4">
                      <div className="text-center">
                          <h5 className="fw-bolder">{item.name}</h5>
                          R{item.price}
                      </div>
                  </div>
                  <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                      <div className="text-center"><a className="btn btn-outline-dark mt-auto" onClick={function()
                      {
                        var myCart = [];
                        if (!props.cart)
                        {
                          props.setCart([...[], item.ID]);
                        }
                        else
                        {
                          props.setCart([...props.cart, item.ID]);
                        }
                      }}>Add to cart</a></div>
                  </div>
              </div>
              </div> 
            );
          })
        }
        </div>
        </div>
        </section>

      </div>
    </>
  )
}
