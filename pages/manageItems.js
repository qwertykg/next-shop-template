import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  const registerUser = async event => {
    event.stopPropagation();
    var name = document.getElementById("name").value;
    var price = document.getElementById("price").value;
    var file = document.getElementById("file").files[0];  
    var imageName = file.name.split(".")[0];

    var result = await fetch(`https://ct6uuziy7c.execute-api.af-south-1.amazonaws.com/get-presigned-url?name=${imageName}`);

    result = await result.json();
    var presignedUrl = result.url;

    await fetch(presignedUrl, {
      method: 'PUT',
      body : file
    });

    var result = await fetch(`https://33wdh58zzb.execute-api.af-south-1.amazonaws.com/uploadShopItem?image=${imageName}&name=${name}&myprice=${price}`, {
      mode: "no-cors"
    });

    document.getElementById("name").value = ""
    document.getElementById("price").value = "";
    document.getElementById("file").value = "";
  }

  return (
    <>
      <Head>
        <title>Online Shop | Manage Items</title>
        <meta name="keywords" content="ninjas"/>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
      </Head>
      <div>

      <div className="file-upload">
      <div className="file-select" className="center">
      <form>
        <input id="file" type="file" className="marginl10"/>
        <br></br>
        <br></br>
        <input id="name" type="text" name="name" placeholder="Item name"/>
        <br></br>
        <br></br>
        <input id="price" type="text" name="price" placeholder="Item price"/>
        <br></br>
        <br></br>
        </form>
        <button onClick={registerUser}>Submit</button>
  </div>
</div>
      </div>
    </>
  )
}