import Footer from "./Footer"
import Navbar from "./Navbar"

const Layout = ({ children, cart}) => {
  return (
    <div className="content">
      <Navbar cart={cart}/>
      { children }
      <Footer />
    </div>
  );
}
 
export default Layout;