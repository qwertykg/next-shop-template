import Link from 'next/link';
import {useSession, signIn, signOut} from "next-auth/client";

const Navbar = ({cart}) => {
  const [session] = useSession();
  if (!cart) cart = [];

  if (session) {
    if (session.user.name === "admin") {
      return (
        <nav>
          <div className="logo">
          <h2>Logo</h2>  
          </div>
          <Link href="/account" className="margin-right"><a>{session.user.name}</a></Link>
          <Link href="/"><a>Home</a></Link>
          <Link href="/cart" className="margin-right" cart={cart}><a>Cart({cart.length})</a></Link>
          <Link href="/manageItems" className="margin-right"><a>Manage items</a></Link>
          <Link href="#"><a onClick={() => signOut()}>Logout</a></Link>
        </nav>
      );
    }
    return (
      <nav>
        <div className="logo">
        <h2>Logo</h2>  
        </div>
        <Link href="/account" className="margin-right"><a>{session.user.name}</a></Link>
        <Link href="/"><a>Home</a></Link>
        <Link href="/cart" className="margin-right" cart={cart}><a>Cart({cart.length})</a></Link>
        <Link href="#"><a onClick={() => signOut()}>Logout</a></Link>
      </nav>
    );  
  }
  else {
    return (
      <nav>
        <div className="logo">
        <h2>Logo</h2>  
        </div>
        <Link href="/"><a>Home</a></Link>
        <Link href="/cart" className="margin-right" cart={cart}><a>Cart({cart.length})</a></Link>
        <Link href="#"><a onClick={() => signIn()}>Login</a></Link>
      </nav>
    );    
  }
}
 
export default Navbar;